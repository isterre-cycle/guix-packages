# ISTerre guix packages

GUIX reference cookbook on channels: https://guix.gnu.org/cookbook/en/html_node/Channels.html

## Using this channel

Edit / create the `~/.config/guix/channels.scm` file.

```
;; Add custom packages to those the default Guix channel provides.
(cons (channel
        ;; ISTerre-cycle
        (name 'ISTerre-cycle)
        (url "https://gricad-gitlab.univ-grenoble-alpes.fr/isterre-cycle/guix-packages.git"))
      %default-channels)
```

then run `guix pull` and update the session's guix profile:

```
GUIX_PROFILE="$HOME/.config/guix/current"
. "$GUIX_PROFILE/etc/profile"
```


## Multiple channels

```
;; Add custom packages to those Guix provides.
(cons* (channel
        (name 'isterre-guix-packages)
        (url "https://gricad-gitlab.univ-grenoble-alpes.fr/isterre-cycle/guix-packages.git"))
       (channel
        (name 'guix-hpc)
        (url "https://gitlab.inria.fr/guix-hpc/guix-hpc"))
       (channel
        (name 'guix-past)
        (url "https://gitlab.inria.fr/guix-hpc/guix-past")
        (introduction
         (make-channel-introduction
          "0c119db2ea86a389769f4d2b9c6f5c41c027e336"
          (openpgp-fingerprint
           "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5"))))
      %default-channels)
```

Alternative

```
(list %default-guix-channel
      (channel
        (name 'guix-science)
        (url "https://github.com/guix-science/guix-science")
        (introduction
         (make-channel-introduction
          "b1fe5aaff3ab48e798a4cce02f0212bc91f423dc"
          (openpgp-fingerprint
           "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))))
      ;(channel
        ;(name 'guix-cran)
        ;(url "https://github.com/guix-science/guix-cran"))
      (channel
        (name 'gricad-guix-packages)
        (url "https://gricad-gitlab.univ-grenoble-alpes.fr/bouttiep/gricad_guix_packages.git"))
      (channel
        (name 'guix-hpc-non-free)
        (url "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free.git"))
      (channel
        (name 'guix-hpc)
        (url "https://gitlab.inria.fr/guix-hpc/guix-hpc")))
```
