(define-module (packages perl-packages)
    #:use-module (srfi srfi-1)
    #:use-module ((guix licenses) #:prefix license:)
    #:use-module (gnu packages)
    #:use-module (guix gexp)
    #:use-module (guix packages)
    #:use-module (guix download)
    #:use-module (guix git-download)
    #:use-module (guix utils)
    #:use-module (guix build-system gnu)
    #:use-module (guix build-system perl)
    #:use-module (guix memoization)
    #:use-module (guix search-paths)
    #:use-module (gnu packages base)
    #:use-module (gnu packages perl)
    #:use-module (gnu packages bash)
    #:use-module (gnu packages compression)
    #:use-module (gnu packages databases)
    #:use-module (gnu packages fontutils)
    #:use-module (gnu packages freedesktop)
    #:use-module (gnu packages gcc)
    #:use-module (gnu packages gd)
    #:use-module (gnu packages gl)
    #:use-module (gnu packages gtk)
    #:use-module (gnu packages hurd)
    #:use-module (gnu packages image)
    #:use-module (gnu packages language)
    #:use-module (gnu packages less)
    #:use-module (gnu packages ncurses)
    #:use-module (gnu packages perl-check)
    #:use-module (gnu packages perl-compression)
    #:use-module (gnu packages perl-maths)
    #:use-module (gnu packages perl-web)
    #:use-module (gnu packages pkg-config)
    #:use-module (gnu packages python)
    #:use-module (gnu packages readline)
    #:use-module (gnu packages sdl)
    #:use-module (gnu packages security-token)
    #:use-module (gnu packages textutils)
    #:use-module (gnu packages video)
    #:use-module (gnu packages web)
    #:use-module (gnu packages xml)
    #:use-module (gnu packages xorg)
    #:export (perl-extutils-pkgconfig))

(define-public perl-local-lib
    (package
        (name "perl-local-lib")
        (version "2.000029")
        (source
            (origin
                (method url-fetch)
                (uri (string-append
                    "mirror://cpan/authors/id/H/HA/HAARG/local-lib-" version
                    ".tar.gz"))
                (sha256
                    (base32
                "1mzx7srjpc090z48f7i5wlcxb1w19cg71ia7bff913jcq487my4d"))))
        (build-system perl-build-system)
        (propagated-inputs (list perl-module-build))
        (home-page "https://metacpan.org/release/local-lib")
        (synopsis "create and use a local lib/ for perl modules with PERL5LIB")
        (description "local::lib lets you build and install your own Perl 
        modules, without having to build and install your own Perl.  
        It uses the system's Perl, as well as as the system's build 
        environment to build modules, which are then installed into your 
        home directory.")
        (license (package-license perl))
    )
)

(define-public perl-statistics-descriptive
    (package
        (name "perl-statistics-descriptive")
        (version "3.0801")
        (source 
            (origin
                (method url-fetch)
                (uri (string-append
                        "mirror://cpan/authors/id/S/SH/SHLOMIF/Statistics-Descriptive-"
                        version ".tar.gz"))
                (sha256
                    (base32
                    "0rcg0aixq4vj7bbwnk7dd2yfpq2mw5c2vzz0d1hr3anw7yk70yq4"))))
        (build-system perl-build-system)
        (native-inputs (list perl-module-build))
        (propagated-inputs (list perl-list-moreutils))
        (home-page "https://metacpan.org/release/Statistics-Descriptive")
        (synopsis "Module of basic descriptive statistical functions.")
        (description "Module of basic descriptive statistical functions.")
        (license license:perl-license)
    )
)

