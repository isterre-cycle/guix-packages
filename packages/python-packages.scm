(define-module (packages python-packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages time)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build))

(define-public python-cdsapi
  (package
    (name "python-cdsapi")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "cdsapi" version))
        (sha256
          (base32 "0va7y3aj1igv9hwbsgx0zkpg7v7axlbica5hs0ai3k4n34pykwqr"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
        (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
          (delete 'sanity-check)
          (delete 'check))))
    ;(propagated-inputs (list python-requests python-tqdm))
    ;(propagated-inputs (list python-tqdm))
    ;(inputs `(("requests" ,python-requests)
    ;                 ("tqdm" ,python-tqdm))) 
    (home-page "https://github.com/ecmwf/cdsapi")
    (synopsis "Climate Data Store API")
    (description "Climate Data Store API")
    (license license:gpl3+)
  )
)

(define-public python-pydot-cycle
(package
  (name "python-pydot-cycle")
  (version "1.4.2")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "pydot" version))
      (sha256
      (base32
        "0z80zwldf7ffkwrpm28hixsiqp3053j7g281xd6phmnbkfiq3014"))))
  (build-system python-build-system)
  (arguments
    '(#:phases
      (modify-phases %standard-phases
      ;; solve sanity-check crash on homeless-shelter not found, ugly but works
      (delete 'sanity-check)
      (delete 'check))))
  (inputs
    (list python-pyparsing graphviz))
  (home-page "https://github.com/pydot/pydot")
  (synopsis "Python interface to Graphviz's DOT language")
  (description
  "Pydot provides an interface to create, handle, modify and process
graphs in Graphviz's DOT language, written in pure Python.")
  (license license:expat)))

(define-public python-asf-cherry-pick
(package
  (name "python-asf-cherry-pick")
  (version "2.0.0")
  (source (origin
            (method url-fetch)
            (uri (pypi-uri "asf_cherry_pick" version))
            (sha256
              (base32
              "0kzfdf93s6613cm5c58wz1gj57ka6iv0w57hp9za6a84ycpzm15c"))))
  (build-system pyproject-build-system)
    (arguments
      '(#:phases
        (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
          (delete 'sanity-check)
          (delete 'check))))
  (inputs `(("python-pygeoif" ,python-pygeoif-0.7) 
          ("python-asf-search" ,python-asf-search)
          ("python-fastkml" ,python-fastkml)
          ("python-lxml" ,python-lxml)
          ("python-remotezip" ,python-remotezip)))
  (home-page "")
  (synopsis "Python package for cherry picking Data into asf")
  (description "Python package for cherry picking Data into asf")
  (license #f))
)

(define-public python-fastkml
(package
  (name "python-fastkml")
  (version "0.12")
  (source (origin
            (method url-fetch)
            (uri (pypi-uri "fastkml" version))
            (sha256
              (base32
              "0f5nv02k9x18014d3pk4bbmfrn49ixkyb2xv8ckh0m5mfwpypn1l"))))
  (build-system pyproject-build-system)
    (arguments
      '(#:phases
        (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
          (delete 'sanity-check)
          (delete 'check))))
  (inputs `(("python-pygeoif" ,python-pygeoif-0.7) 
          ("python-dateutil" ,python-dateutil)))
  (home-page "https://github.com/cleder/fastkml")
  (synopsis "Fast KML processing in python")
  (description "Fast KML processing in python")
  (license #f))
)

(define-public python-asf-search
(package
  (name "python-asf-search")
  (version "6.6.2")
  (source (origin
            (method url-fetch)
            (uri (pypi-uri "asf_search" version))
            (sha256
              (base32
              "0gl4jsjagmp9682wl7wz9g31vdh0c4zhk3pnq09m9gi0cfj0l8ak"))))
  (build-system pyproject-build-system)
    (arguments
      '(#:phases
        (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
          (delete 'sanity-check)
          (delete 'check))))
  (inputs (list python-dateparser
                          python-importlib-metadata
                          python-numpy
                          python-dateutil
                          python-pytz
                          python-remotezip
                          python-requests
                          python-shapely
                          python-tenacity))
  (native-inputs (list python-coverage
                      python-ipykernel
                      python-nbconvert
                      python-nbformat
                      python-pytest
                      python-pytest-automation
                      python-pytest-cov
                      python-pytest-xdist
                      python-requests-mock))
  (home-page "https://github.com/asfadmin/Discovery-asf_search.git")
  (synopsis "Python wrapper for ASF's SearchAPI")
  (description "Python wrapper for ASF's @code{SearchAPI}")
  (license license:bsd-3))
)

(define-public python-remotezip
(package
  (name "python-remotezip")
  (version "0.12.1")
  (source (origin
            (method url-fetch)
            (uri (pypi-uri "remotezip" version))
            (sha256
              (base32
              "1v4ds7x6y0ahq10p360lmj8mfzwnj8sh40nl1saxh9aw1j8vfrff"))))
  (build-system pyproject-build-system)
    (arguments
      '(#:phases
        (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
          (delete 'sanity-check)
          (delete 'check))))
  (inputs (list python-requests python-tabulate))
  (home-page "https://github.com/gtsystem/python-remotezip")
  (synopsis
  "Access zip file content hosted remotely without downloading the full file.")
  (description
  "Access zip file content hosted remotely without downloading the full file.")
  (license license:expat))
)

(define-public python-pytest-automation
(package
  (name "python-pytest-automation")
  (version "2.0.1")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "pytest-automation" version))
      (sha256
        (base32
          "14xfyc2l193liz60msdzfyx0s5sasjli7jmdyzdppzy6hchxm362"))))
  (build-system python-build-system)
    (arguments
      '(#:phases
        (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
          (delete 'sanity-check)
          (delete 'check))))
  (inputs
    `(("python-pytest" ,python-pytest)
      ("python-pyyaml" ,python-pyyaml)))
  (home-page
    "https://github.com/asfadmin/Discovery-PytestAutomation")
  (synopsis
    "pytest plugin for building a test suite, using YAML files to extend pytest parameterize functionality.")
  (description
    "pytest plugin for building a test suite, using YAML files to extend pytest parameterize functionality.")
  (license license:bsd-3))
)

(define-public python-pygeoif
  (package
    (name "python-pygeoif")
    (version "1.0.0")
    (source
        (origin
            (method url-fetch)
            (uri (pypi-uri "pygeoif" version))
            (sha256
              (base32
                "17fk9gdc79my8gwli1v92lbq7w9nn7q1s11x4277nqqbidnmk42w"))))
    (build-system python-build-system)
    (arguments
      '(#:phases
        (modify-phases %standard-phases
          ;; solve sanity-check crash on homeless-shelter not found, ugly but works
          (delete 'sanity-check)
          (delete 'check))))
    (inputs
      `(("python-typing-extensions"
        ,python-typing-extensions)))
    (home-page "https://github.com/cleder/pygeoif/")
    (synopsis
      "A basic implementation of the __geo_interface__")
    (description
      "A basic implementation of the __geo_interface__")
    (license #f)
  )
)

(define-public python-pygeoif-0.7
  (package
    (name "python-pygeoif")
    (version "0.7")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "pygeoif" version))
              (sha256
                (base32
                "017yb81482h7197p9wvkm0ihigjahgx3w4mxyd5gmgiq3l3hrahi"))))
    (build-system python-build-system)
    (arguments
    '(#:phases
      (modify-phases %standard-phases
        ;; solve sanity-check crash on homeless-shelter not found, ugly but works
        (delete 'sanity-check)
        (delete 'check))))
    (inputs (list python-typing-extensions))
    (home-page "https://github.com/cleder/pygeoif/")
    (synopsis "A basic implementation of the __geo_interface__")
    (description
    "This package provides a basic implementation of the __geo_interface__")
    (license #f)
  )
)


(define-public python-dask-jobqueue
    (package
        (name "python-dask-jobqueue")
        (version "0.8.2")
        (source
            (origin
                (method url-fetch)
                (uri (pypi-uri "dask-jobqueue" version))
                (sha256
                    (base32
                    "0jli8gg9qr75nq9x0mwqabn5nlzqp54yg2lmbmy38d05bah0fm6k"))))
        (build-system python-build-system)
        (arguments
        `(#:tests? #f
          #:phases
          (modify-phases %standard-phases
            (delete 'tests)
            ;; solve sanity-check crash on homeless-shelter not found, ugly but works
            (delete 'sanity-check)
            (delete 'check))))
        (inputs (list python-dask python-distributed))
        (native-inputs (list python-cryptography python-pytest python-pytest-asyncio))
        (home-page "https://jobqueue.dask.org")
        (synopsis "Deploy Dask on job queuing systems like PBS, Slurm, SGE or LSF")
        (description "Deploy Dask on job queuing systems like PBS, Slurm, SGE or LSF")
        (license #f)
    )
)

